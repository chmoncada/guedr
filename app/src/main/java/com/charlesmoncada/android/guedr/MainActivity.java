package com.charlesmoncada.android.guedr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = MainActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.v(MainActivity.TAG,"Hemos pasado por onCreate");

        Button changeSystemButton = (Button) findViewById(R.id.spanish_button);
        changeSystemButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.v(MainActivity.TAG, "Botón pulsado");
        // Vamos a acceder a la imagen a traves de su nombre
        ImageView weatherImage = (ImageView) findViewById(R.id.weather_system_image);
        // Cambiamos la imagen a mostrar
        weatherImage.setImageResource(R.drawable.offline_weather2);
    }
}
